# 2206 Trust Framework payload examples

These examples matches the requirements for the TrustFramework 2206. They are VerifiableCredentials.
They are to be used on `/2206-unreleased` deployment as the key used to sign the VCs is not a trusted anchors in a non-sandbox environment.