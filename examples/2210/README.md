# 2210 Trust Framework payload examples

These examples matches the requirements for the TrustFramework 2210. They are VerifiablePresentation containing one or several VerifiableCredentials, sometimes linked between.
They are to be used on `/main` or `/development` deployment as the key used to sign the VCs is not a trusted anchors in a non-sandbox environment.